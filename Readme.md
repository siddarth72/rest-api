# REST API

After Downlaoding run installation

## Installation

```
npm install
```

## If you are using mongdb compass then:

Run your mongod.exe in this root folder.
If you get any error then create a folder named "data" and run mongod.exe in this root folder.

## If you want to use mongodb atlas(cloud version) then:

[Check This Link](https://medium.com/featurepreneur/connect-mongodb-database-to-express-server-step-by-step-53e548bb4967)

## Modules Used

- ExpressJs
- NodeJs
- MongoDB
