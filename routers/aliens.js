const express = require('express');
const router = express.Router();
const Alien = require('../models/alien.js');

router.get('/', async (req, res) => {
    try {
        const aliens = await Alien.find();
        res.status(200).json({
            success: true,
            aliens
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const alien = await Alien.findById(req.params.id);
        if (!alien) {
            return res.status(404).json({
                success: false,
                message: 'Alien not found'
            });
        }
        res.status(200).json({
            success: true,
            alien
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        });
    }
});

router.post('/', async (req, res) => {
    try {
        const alien = new Alien({
            name: req.body.name,
            tech: req.body.tech,
            sub: req.body.sub,
        });
        const newAlien = await alien.save();
        res.status(201).json({
            success: true,
            newAlien
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        });
    }
});

router.patch('/:id', async (req, res) => {
    try {
        const alien = await Alien.findById(req.params.id);
        alien.sub = req.body.sub;
        const updatedAlien = await alien.save();
        res.status(200).json({
            success: true,
            updatedAlien
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        });
    }
});

module.exports = router;